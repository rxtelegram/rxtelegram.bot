# RxTelegram.Bot

[![NuGet](http://img.shields.io/nuget/v/RxTelegram.Bot.svg)](https://www.nuget.org/packages/RxTelegram.Bot)
[![Downloads](https://img.shields.io/nuget/dt/RxTelegram.Bot.svg)](https://www.nuget.org/packages/RxTelegram.Bot)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=RxTelegram_RxTelegram.Bot&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=RxTelegram_RxTelegram.Bot)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=RxTelegram_RxTelegram.Bot&metric=coverage)](https://sonarcloud.io/summary/new_code?id=RxTelegram_RxTelegram.Bot)

RxTelegram.Bot supports Telegram Bot API 6.9 (as at September 22, 2023).

This is a reactive designed .NET Library for the Telegram Bot API. It works with the official [Reactive Extentions](https://github.com/dotnet/reactive).

## Examples
Example bot implementations can be found at [RxTelegram.Bot.Examples](https://github.com/RxTelegram/RxTelegram.Bot.Examples).
