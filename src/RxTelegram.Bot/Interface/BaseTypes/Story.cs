namespace RxTelegram.Bot.Interface.BaseTypes;

/// <summary>
/// This object represents a message about a forwarded story in the chat. Currently holds no information.
/// </summary>
public class Story
{
}
